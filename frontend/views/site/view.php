<?php

use kartik\detail\DetailView;

/* @var $this yii\web\View */

?>
<div class="lib-region-view">
    <?= DetailView::widget([
        'model' => $model,
        'hAlign' => 'ALIGN_LEFT',
        'attributes' => [
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'
        ],
    ]) ?>

</div>
