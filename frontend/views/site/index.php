<?php

/* @var $this yii\web\View */

use yii\web\JqueryAsset;

$this->registerJsFile('@web/js/test.js', ['depends' => [JqueryAsset::className()]]);
$this->registerCssFile('@web/css/test.css', ['depends' => [JqueryAsset::className()]]);
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>GridView yii2-grid</h2>
                <p><?= \yii\helpers\Html::a('GridView yii2-grid &raquo;', ['site/grid-view'], ['class' => 'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Modal Pjax</h2>
                <p><?= \yii\helpers\Html::a('Modal Pjax &raquo;', ['site/modal-pjax'], ['class' => 'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>ListView yii2-list-view</h2>
                <p><?= \yii\helpers\Html::a('List View &raquo;', ['site/list-view'], ['class' => 'btn btn-default']) ?></p>
            </div>
        </div>

    </div>
</div>
