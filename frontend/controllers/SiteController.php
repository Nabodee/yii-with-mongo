<?php

namespace frontend\controllers;


use common\models\User;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\search\TestSearch;
use frontend\models\Test;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['signup', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'logout', 'grid-view', 'modal-pjax',
                            'create-view', 'create', 'view', 'update-view', 'update', 'delete', 'list-view'],
                        'allow' => true,
                        'roles' => ['@'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['site/login']);
                        }
                    ],
                    [
                        'actions' => ['about'],
                        'allow' => AuthController::auth([User::ROLE_ADMIN, User::ROLE_USER, User::ROLE_ADMINS]),
                        'roles' => ['@'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['site/index']);
                        }
                    ],
                    [
                        'actions' => ['contact'],
                        'allow' => AuthController::auth([User::ROLE_ADMIN]),
                        'roles' => ['@'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['site/index']);
                        }
                    ],
                    [
                        'actions' => ['signup', 'login'],
                        'allow' => false,
                        'roles' => ['@'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['site/about']);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['site/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['site/index']);
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['site/login']);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            $modelLogin = new LoginForm();
            $modelLogin->username = $model->username;
            $modelLogin->password = $model->password;
            if ($modelLogin->login()) {
                return $this->redirect(['site/index']);
            }
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }


    public function actionGridView()
    {
        $searchModel = new TestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('grid-view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListView()
    {
        $searchModel = new TestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $TestSearch = Yii::$app->request->get('TestSearch');
//        return Json::encode($TestSearch);
        if($TestSearch['page_size']){
            $dataProvider->pagination->pageSize = $TestSearch['page_size'];
        }else{
            $dataProvider->pagination->pageSize = 5;
        }

        return $this->render('list-view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionModalPjax()
    {
        $searchModel = new TestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('modal-pjax', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateView()
    {
        if (Yii::$app->request->isAjax) {
            $model = new Test();
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        }
        return $this->redirect(['site/index']);
    }

    public function actionCreate()
    {
        $model = new Test();
        if ($model->load(Yii::$app->request->post())) {
            try {
                if ($model->save()) {
                    return Json::encode(['status' => true, 'data' => [], 'message' => '', 'errorMessage' => '']);
                }
            } catch (\Exception $e) {
                return Json::encode(['status' => false, 'data' => [], 'message' => '', 'errorMessage' => $e->getMessage()]);
            }
        }
        return Json::encode(['status' => false, 'data' => [], 'message' => '', 'errorMessage' => '']);
    }

    public function actionView()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => Test::find()->where(['_id' => Yii::$app->request->post('id')])->one(),
            ]);
        }
        return $this->redirect(['site/index']);
    }

    public function actionUpdateView()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => Test::find()->where(['_id' => Yii::$app->request->post('id')])->one(),
            ]);
        }
        return $this->redirect(['site/index']);
    }

    public function actionUpdate()
    {
        $modelTest = Yii::$app->request->post('Test');
        $model = Test::find()->where(['_id' => $modelTest['_id']])->one();
        if ($model->load(Yii::$app->request->post())) {
            try {
                if ($model->save()) {
                    return Json::encode(['status' => true, 'data' => [], 'message' => '', 'errorMessage' => '']);
                }
            } catch (\Exception $e) {
                return Json::encode(['status' => false, 'data' => [], 'message' => '', 'errorMessage' => $e->getMessage()]);
            }
        }
    }

    public function actionDelete()
    {
        $model = Test::find()->where(['_id' => Yii::$app->request->post('id')])->one();
        try {
            if ($model->delete()) {
                return Json::encode(['status' => true, 'data' => [], 'message' => '', 'errorMessage' => '']);
            }
        } catch (\Exception $e) {
            return Json::encode(['status' => false, 'data' => [], 'message' => '', 'errorMessage' => $e->getMessage()]);
        }
    }

}
