<?php

namespace frontend\controllers;

use Yii;

class AuthController
{

    public static function auth($item)
    {
        if (!Yii::$app->user->isGuest) {
            if (in_array(Yii::$app->user->identity->role, $item)) {
                return true;
            }
        }
        return false;
    }

}
