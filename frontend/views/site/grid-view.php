<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use frontend\models\Test;

/* @var $this yii\web\View */


$this->title = 'My Yii Application';
?>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(['id' => 'pjax_index']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '-'
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['style' => 'min-width:40px;width:40px']],
            [
                'attribute' => 'a',
                'headerOptions' => ['style' => 'min-width:260px;'],
            ],
            [
                'attribute' => 'b',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Test::getBArrayList(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => [
                    'placeholder' => 'ค้นหา b',
                    'multiple' => true,
                ],
                'headerOptions' => ['style' => 'min-width:260px;'],
                'value' => function ($model) {
                    return $model->b;
                }
            ],
            [
                'attribute' => 'c',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Test::getCArrayList(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => [
                    'placeholder' => 'ค้นหา c',
                    'multiple' => true,
                ],
                'headerOptions' => ['style' => 'min-width:260px;'],
            ],
            [
                'attribute' => 'de',
                'label' => 'ค้นหา D และ E',
                'headerOptions' => ['style' => 'min-width:260px;'],
                'value' => function ($model) {
                    return $model->d . ' ' . $model->e;
                }
            ],
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
