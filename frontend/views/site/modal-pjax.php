<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\web\JqueryAsset;
use yii\widgets\Pjax;
use frontend\models\Test;

/* @var $this yii\web\View */
$this->registerJsFile('@web/js/modalTest.js', ['depends' => [JqueryAsset::className()]]);

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('<i class="fa fa-plus"></i> ' . 'เพิ่ม', ['class' => 'btn btn-success', 'id' => 'create-data']) ?>
    </p>
    <?php Pjax::begin(['id' => 'pjax_index']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '-'
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['style' => 'min-width:40px;width:40px']],
            [
                'attribute' => 'a',
                'headerOptions' => ['style' => 'min-width:260px;'],
            ],
            [
                'attribute' => 'b',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Test::getBArrayList(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => [
                    'placeholder' => 'ค้นหา b',
                ],
                'headerOptions' => ['style' => 'min-width:260px;'],
            ],
            [
                'attribute' => 'c',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Test::getCArrayList(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => [
                    'placeholder' => 'ค้นหา c',
                    'multiple' => true,
                ],
                'headerOptions' => ['style' => 'min-width:260px;'],
            ],
            [
                'attribute' => 'de',
                'label' => 'ค้นหา D และ E',
                'headerOptions' => ['style' => 'min-width:260px;'],
                'value' => function ($model) {
                    return $model->d . ' ' . $model->e;
                }
            ],
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ดำเนินการ',
                'headerOptions' => ['style' => 'text-align: center;width:90px;min-width:90px'],
                'template' => '<div class="btn-group btn-group-sm text-center" style="display: block;text-align: center;" role="group">{view} {update} {delete}</div>',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<i class="glyphicon glyphicon-eye-open blue"></i>', ['site/modal-pjax'], [
                            'class' => 'view',
                            'style' => 'margin:0px 3px',
                            'data-id' => $key,
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<i class="glyphicon glyphicon-pencil green"></i>', ['site/modal-pjax'], [
                            'class' => 'update',
                            'style' => 'margin:0px 3px',
                            'data-id' => $key,
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<i class="glyphicon glyphicon-trash red"></i>', ['site/modal-pjax'], [
                            'class' => 'delete',
                            'style' => 'margin:0px 3px',
                            'data-id' => $key,
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
<?php Modal::begin([
    'id' => 'control-modal',
    'header' => '<h4 class="modal-title"></h4>',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => true],
    'footer' => null,
    'options' => [
        'tabindex' => false,
    ],
]);
Modal::end();
?>
