/*globals $:false */

$(document).delegate('#create-data', 'click', function () {
    $.ajax({
        url: 'create-view',
        type: 'GET',
        beforeSend: function () {

        },
    }).done(function (data) {
        $('.modal-body').html(data);
        $('.modal-title').html('เพิ่ม');
        $('#control-modal').modal('show');
    });
});

$(document).delegate('#create-form', 'submit', function (event) {
    event.preventDefault();
    let data = $(this).serialize();
    let _id = $('#test-_id').val();
    let url = 'create';
    if (_id) {
        url = 'update';
    }
    $.ajax({
        url: url,
        type: 'POST',
        data: data,
    }).done((data) => {
        let result = JSON.parse(data);
        console.log(result);
        if (result.status == true) {
            $.pjax.reload({container: '#pjax_index', 'timeout': 5000}).done(function () {

                $('#control-modal').modal('toggle');

            });
        } else {
            $.pjax.reload({container: '#pjax_index', 'timeout': 5000}).done(function () {
                alert('เกิดข้อผิดพลาด');
                $('#control-modal').modal('toggle');

            });
        }

    });
});

$(document).delegate('.view', 'click', function (event) {
    event.preventDefault();
    let id = $(this).attr('data-id');
    let token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        type: 'POST',
        url: 'view',
        data: {
            id: id,
            _csrf: token
        },
        beforeSend: function () {

        }
    }).done(function (data) {
        $('.modal-body').html(data);
        $('.modal-title').html('รายละเอียด');
        $('#control-modal').modal('show');
    });
});

$(document).delegate('.update', 'click', function (event) {
    event.preventDefault();
    let id = $(this).attr('data-id');
    let token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: 'update-view',
        data: {
            id: id,
            _csrf: token
        },
        beforeSend: function () {

        },
    }).done(function (data) {
        $('.modal-body').html(data);
        $('.modal-title').html('แก้ไข');
        $('#control-modal').modal('show');
    });
});

$(document).delegate('.delete', 'click', function (event) {
    event.preventDefault();
    let id = $(this).attr('data-id');
    let token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: 'delete',
        data: {
            id: id,
            _csrf: token
        },
        beforeSend: function () {

        },
    }).done(function (data) {
        let resault = JSON.parse(data);
        console.log(resault);
        if (resault.status == true) {
            $.pjax.reload({container: '#pjax_index', 'timeout': 5000}).done(function () {
                alert('ลบข้อมูลสำเร็จ');
            });
        } else {
            $.pjax.reload({container: '#pjax_index', 'timeout': 5000}).done(function () {
                alert('เกิดข้อผิดพลาด');
            });
        }
    });
});
