<?php

namespace frontend\models;


use yii\helpers\ArrayHelper;
use yii\mongodb\ActiveRecord;

/**
 * Test model
 */
class Test extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function CollectionName()
    {
        return ['yii2_test', 'test'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'], 'string'],
        ];
    }

    public function attributes()
    {
        return ['_id', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'];
    }


    public static function getBArrayList()
    {
        return ArrayHelper::map(Test::find()->distinct('b'),
            function ($model) {
                return $model;
            }
            , function ($model) {
                return 't-' . $model;
            });
    }

    public static function getCArrayList()
    {
        return ArrayHelper::map(
            self::find()->distinct('c'),
            function ($model) {
                return $model;
            },
            function ($model) {
                return $model;
            });
    }


}
