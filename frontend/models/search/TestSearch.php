<?php

namespace frontend\models\search;

use frontend\models\Test;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RiceInformationSearch represents the model behind the search form of `app\modules\o6\models\RiceInformation`.
 */
class TestSearch extends Test
{

    public $de;
    public $key;
    public $global_search;
    public $page_size;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['a', 'de', 'f', 'g', 'h', 'i', 'j', 'k', 'key', 'global_search'], 'string'],
            [['b', 'c',], 'safe'],
            [['page_size'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Test::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
//            'pagination' => [
//                'pageSize' => 20
//            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $conditionDE = ['or'];
        foreach (explode(' ', $this->de) as $key) {
            $conditionDE[] = ['like', 'e', $key];
            $conditionDE[] = ['like', 'd', $key];
        }

        $conditionGlobalSearch = ['or'];
        foreach (explode(' ', $this->global_search) as $key) {
            $conditionGlobalSearch[] = ['like', 'a', $key];
            $conditionGlobalSearch[] = ['like', 'b', $key];
            $conditionGlobalSearch[] = ['like', 'c', $key];
            $conditionGlobalSearch[] = ['like', 'd', $key];
            $conditionGlobalSearch[] = ['like', 'e', $key];
        }

        $query->andFilterWhere([
            'b' => $this->b,
            'c' => $this->c
        ]);

        $query->andFilterWhere(['like', 'a', $this->a])
            ->andFilterWhere($conditionDE)
            ->andFilterWhere($conditionGlobalSearch);

        return $dataProvider;
    }
}
