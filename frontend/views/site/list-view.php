<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use kartik\select2\Select2;

/* @var $this yii\web\View */


$this->title = 'My Yii Application';
?>
<style>
    .my-content {
        height: 80px;
        border-radius: 5px;
        border: solid 1px #eeeeee;
        margin: 10px;
        box-shadow: 0px 0px 5px rgba(193, 193, 193, 0.65);
        display: flex;
        padding: 20px;
    }

    .my-topic {
        display: flex;
        align-self: center;
        width: 20%;
    }
</style>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'create-form']); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($searchModel, 'global_search',['options'=>['style'=>'display:inline-block;width:79%']])->textInput() ?>
            <div class="form-group" style="display:inline-block;width: 20%">
                <?= Html::button('Search', ['class' => 'btn btn-success', 'id' => 'my-search']) ?>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($searchModel, 'page_size',['options'=>['style'=>'width:100px']])->widget(Select2::classname(), [
                'data' => [2 => 2, 5 => 5, 10 => 10, 15 => 15, 20 => 20],
                'options' => ['placeholder' => 'pageSize'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label('Page Size');
            ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::begin(['id' => 'pjax_index']); ?>
    <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_list-view-item',
        'summary' => '',
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
<?php
$js = <<<JS
$(document).delegate('#testsearch-page_size', 'change',function (event) {
    event.preventDefault();
    let pageSize = $('#testsearch-page_size').val();
    let keyword = $('#testsearch-global_search').val();
    $.pjax.reload({
        container: '#pjax_index',
        'timeout': 2500,
        'url': 'list-view?TestSearch[global_search]=' + keyword + '&TestSearch[page_size]=' + pageSize
    });
});

$(document).delegate('#my-search', 'click',function (event) {
    event.preventDefault();
    let pageSize = $('#testsearch-page_size').val();
    let keyword = $('#testsearch-global_search').val();
    $.pjax.reload({
    container: '#pjax_index',
        'timeout': 2500,
        'url': 'list-view?TestSearch[global_search]=' + keyword + '&TestSearch[page_size]=' + pageSize
    });
});
JS;
$this->registerJs($js);
?>
