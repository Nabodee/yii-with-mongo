<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="o1-rainfall-form">
    <?php $form = ActiveForm::begin(['id' => 'create-form']); ?>
    <?= Html::activeHiddenInput($model, '_id'); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'a')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'b')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'c')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'd')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'e')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'f')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'g')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'h')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'i')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'j')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'k')->textInput() ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
