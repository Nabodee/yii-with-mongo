<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use frontend\models\Test;

/* @var $this yii\web\View */


$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <div class="my-content">
                    <p class="my-topic">A : <?= $model->a ?></p>
                    <p class="my-topic">B : <?= $model->b ?></p>
                    <p class="my-topic">C : <?= $model->c ?></p>
                    <p class="my-topic">D : <?= $model->d ?></p>
                    <p class="my-topic">E : <?= $model->e ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
